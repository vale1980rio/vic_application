﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RedLine
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
    public class BootReceiver : BroadcastReceiver
    {


       


        public override void OnReceive(Context context, Intent intent)
        {
            //Intent i = new Intent(context, typeof(MainActivity));
            //i.AddFlags(ActivityFlags.NewTask);
            //context.StartActivity(i);

            //Intent serviceStart = new Intent(context, typeof(PhoneCallService));
            //context.StartService(serviceStart);

            Intent serviceStart = new Intent(context, typeof(PhoneCallService));

            //Vedo su quale versione di android gira l'applicazione, se oreo ho un comportamento diverso.
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                context.StartForegroundService(serviceStart);
            }
            else
            {
                context.StartService(serviceStart);
            }




        }
    }

}