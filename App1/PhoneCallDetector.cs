﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Preferences;
using Android.Provider;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;
using SQLite;

namespace RedLine
{
    public class PhoneCallDetector : PhoneStateListener
    {
        bool AlertActivityOpened = false;
    
        readonly Context context;
        public MediaPlayer player;

        public void Close()
        {
            player.Stop();
        }

        public void StartPlayer(string filePath)
        {
            if (player == null)
            {
                player = new MediaPlayer();
            }
            AudioManager am = (AudioManager)Android.App.Application.Context.GetSystemService(Context.AudioService);

            int maxVol = am.GetStreamMaxVolume(Android.Media.Stream.Music);
            am.SetStreamVolume(Android.Media.Stream.Music, maxVol, VolumeNotificationFlags.PlaySound);
            am.RingerMode = RingerMode.Normal;
            // player.SetVolume(maxVol, maxVol);
            player.Reset();
            player.SetDataSource(filePath);
            player.Prepare();
            //player.Looping = true;
            player.Start();


        }

        public PhoneCallDetector(Context context_)
        {
            this.context = context_;
        }

      

        public override void OnCallStateChanged(CallState state, string incomingNumber)
        {

            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            bool test = prefs.GetBoolean("alarm_key", false);
            

            /*if (state == CallState.Ringing)
            {
            */
            //System.Console.WriteLine("numero " + incomingNumber);
            //ShowNotification("STATO = " + state + " numero " + incomingNumber);
            Msqlite mm = new Msqlite();
            //mm.Insert_Log("cambio di stato");
            

            if (incomingNumber != "")
            {
                mm.Insert_Number(incomingNumber, state.ToString());
                if (mm.Check_Number())
                {

                   
                    if (test==false)
                    {
                        
                        ShowNotification("Chiama urgentemente il numero prioritario!!!");

                        Intent intent = new Intent();
                        intent.AddFlags(ActivityFlags.NewTask);

                        intent.SetClass(Android.App.Application.Context, typeof(AlarmManager));
                        Android.App.Application.Context.StartActivity(intent);

                        //provare startActivityForResult testando il result della activity alarm


                    }




                }

            }
            //GetCallLogs();
            AlertActivityOpened = mm.Check_Alarm();
            if (!AlertActivityOpened)
            {
                
                ISharedPreferencesEditor editor = prefs.Edit();
                editor.PutBoolean("alarm_key", false);
                // editor.Commit();    // applies changes synchronously on older APIs
                editor.Apply();        // applies changes asynchronously on newer APIs

            }
            base.OnCallStateChanged(state, incomingNumber);



            /*
        }
        */
        }

        private void MessageOREO(string message)
        {
            var channelId = "my_service";
            var channelName = "My Background Service";
            NotificationChannel chan = new NotificationChannel(channelId, channelName, Android.App.NotificationImportance.Max);
            chan.LightColor = Color.Blue;
            chan.LockscreenVisibility = NotificationVisibility.Secret;
            NotificationManager service = (NotificationManager)this.context.GetSystemService(Context.NotificationService);
            service.CreateNotificationChannel(chan);


            Notification.Builder builder = new Notification.Builder(this.context, channelId);
            Notification notification = builder
                .SetOngoing(true)
                .SetPriority((int)NotificationPriority.Max)
                .SetContentTitle("RedLine")
               .SetSmallIcon(Resource.Mipmap.ic_launcher)
                .SetContentText(message)
                .SetCategory(Notification.CategoryService)
                .Build();

            service.Notify(101, builder.Build());

        }
        private void ShowNotification(string message)
        {
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                MessageOREO(message);
            }
            else
            {
                Notification.Builder builder = new Notification.Builder(this.context)
                    .SetContentTitle("RedLine")
                        .SetSmallIcon(Android.Resource.Drawable.SymActionCall)
                        .SetContentText(message)
                        ;

                NotificationManager notificationManager = (NotificationManager)this.context.GetSystemService(Context.NotificationService);
                notificationManager.Notify(101, builder.Build());
            }
        }

        public void GetCallLogs()
        {

            // filter call logs by type = missed
            // string queryFilter = String.Format ("{0}={1}", CallLog.Calls.Type, (int)CallType.Missed);

            // filter in desc order limit by 3
            // string querySorter = String.Format ("{0} desc limit 3", CallLog.Calls.Date);

            // filter in desc order limit by no
            string querySorter = String.Format("{0} desc ", CallLog.Calls.Date);

            // CallLog.Calls.ContentUri is the path where data is saved
            Android.Database.ICursor queryData = Application.Context.ContentResolver.Query(CallLog.Calls.ContentUri, null, null, null, querySorter);

            while (queryData.MoveToNext())
            {
                //---phone number---
                string callNumber = queryData.GetString(queryData.GetColumnIndex(CallLog.Calls.Number));

                //---date of call---
                string callDate = queryData.GetString(queryData.GetColumnIndex(CallLog.Calls.Date));

                //---1-incoming; 2-outgoing; 3-missed---
                String callType = queryData.GetString(queryData.GetColumnIndex(CallLog.Calls.Type));

            }



        }
    }
    public class Msqlite
        {


            [Table("Num_Telefono")]
            public class Numeri_Telefono
            {
                //[PrimaryKey, AutoIncrement, Column("_id")]
                //public int Id { get; set; }
                //[MaxLength(8)]
                public string Number { get; set; }

                public string State { get; set; }

                public DateTime Orario { get; set; }
            }
            [Table("Logger")]
            public class Logger
            {
                public string Testo { get; set; }
                public DateTime Data { get; set; }

            }

            SQLiteConnection db;

            private void Conn()
            {
                var dbName = "Numbers.db3";
                var path = System.IO.Path.Combine("/sdcard/", dbName);
                db = new SQLiteConnection(path);
            }


            public void Insert_Number(string Number, string State)
            {
                Conn();
                db.CreateTable<Numeri_Telefono>();


                string query = string.Format("INSERT INTO  Num_Telefono (State,Number,Orario) VALUES ('{0}','{1}',DATETIME('now')) ", State, Number);

                db.Query<Numeri_Telefono>(query);

            }



            public void Insert_Log(string Testo)
            {
                Conn();
                db.CreateTable<Logger>();


                string query = string.Format("INSERT INTO  Logger (Testo,data) VALUES ('{0}',DATETIME('now')) ", Testo);

                db.Query<Logger>(query);

            }

            public bool Check_Number()
            {
                Conn();
                int chiamate = 0;

                string PrioritarioSetted;

                PrioritarioSetted = db.ExecuteScalar<string>("select numero from PriorityNumbers limit 1");
                if (PrioritarioSetted =="")
                {
                    PrioritarioSetted = "+393475478554";

                }

                chiamate = db.ExecuteScalar<int>("select count(*) as Chiamate from Num_Telefono " +
                    "where state='Idle' and Orario>= datetime('now','-5 minute') and number='" +
                    PrioritarioSetted +
                    "'" +
                    //verifica eventuale risposta o richiamata
                    " and not exists (" +
                    "select 1 from Num_Telefono " +
                    "where state='Offhook' and Orario>= datetime('now','-5 minute') and number='"+
                    PrioritarioSetted+
                    "'" +
                    ") " +
                    "group by Number");
                //ShowNotification2("NUmero chiamate = " + chiamate.ToString());
                if (chiamate >= 5) return true; else return false;

            }

            public bool Check_Alarm()
            {
            Conn();
            int AllarmeStoppato=0;

            AllarmeStoppato = db.ExecuteScalar<int>("select count(*) as conteggio from alarmsact where Orario>= datetime('now','-2 minute')");

            if (AllarmeStoppato == 0)
                {
                    return false;
                }
            else
                {
                    return true;
                }
            }
    }
}
 