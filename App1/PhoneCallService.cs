﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;

namespace RedLine
{
    [Service]
    public class PhoneCallService : Service
    {
        public const int SERVICE_RUNNING_NOTIFICATION_ID = 10000;
        public const string PRIMARY_CHANNEL = "default";
        
        public override void OnCreate()
        {
            base.OnCreate();
        }
        
        //this will not be called, however it is require to override
        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }
        
        private string createNotificationChannel()
        {
            var channelId = "my_service";
            var channelName = "My Background Service";
            NotificationChannel chan = new NotificationChannel(channelId, channelName, Android.App.NotificationImportance.Max);
            chan.LightColor = Color.Blue;
            chan.LockscreenVisibility = NotificationVisibility.Secret;
            NotificationManager service = (NotificationManager)this.GetSystemService(Context.NotificationService);
            service.CreateNotificationChannel(chan);
            return channelId;
        }



        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            base.OnStartCommand(intent, flags, startId);


            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                var callDetector = new PhoneCallDetector(this);

                var tm = (TelephonyManager)base.GetSystemService(TelephonyService);
                tm.Listen(callDetector, PhoneStateListenerFlags.CallState);

                Notification.Builder builder = new Notification.Builder(this, createNotificationChannel());
                Notification notification = builder
                    .SetOngoing(true)
                    .SetPriority((int)NotificationPriority.Max)
                    .SetContentTitle("Ginevra")
                    .SetContentText("Il listner sta girando sulle call")
                    .SetCategory(Notification.CategoryService)
                    .Build();
                StartForeground(SERVICE_RUNNING_NOTIFICATION_ID, notification);
            }
            else
            {
                var callDetector = new PhoneCallDetector(this);
                var tm = (TelephonyManager)base.GetSystemService(TelephonyService);
                tm.Listen(callDetector, PhoneStateListenerFlags.CallState);
                var notification = new Notification.Builder(this).SetContentTitle("Ginevra").SetContentText("Il listner sta girando sulle call").SetOngoing(true).Build();

                //.SetContentTitle(Resources.GetString(Resource.String.app_name))


                // Enlist this instance of the service as a foreground service
                StartForeground(SERVICE_RUNNING_NOTIFICATION_ID, notification);
            }

            return StartCommandResult.Sticky;
        }
    }


}