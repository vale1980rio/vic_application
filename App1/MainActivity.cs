﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Content;
using Android.Media;
using static Android.Provider.MediaStore;
using SQLite;
using System;

namespace RedLine
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button Aggiorna;
        EditText PNum;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_main);

            Intent serviceStart = new Intent(this, typeof(PhoneCallService));
            
            //Vedo su quale versione di android gira l'applicazione, se oreo ho un comportamento diverso.
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                this.StartForegroundService(serviceStart);
            }
            else
            {
                this.StartService(serviceStart);
            }
            
            NotificationManager notificationManager = (NotificationManager)this.GetSystemService(Context.NotificationService);
            if (!notificationManager.IsNotificationPolicyAccessGranted)
            {
                StartActivityForResult(new Intent("android.settings.NOTIFICATION_POLICY_ACCESS_SETTINGS"), 0);
                
            }

            PNum=FindViewById<EditText>(Resource.Id.edtNumeroPrioritario);

            Aggiorna = FindViewById<Button>(Resource.Id.btnAggiornaNumero);
            Aggiorna.Click += Aggiorna_Click;

        }

        private void Aggiorna_Click(object sender, System.EventArgs e)
        {
            SqliteManagerRL mm = new SqliteManagerRL();
            mm.Insert_PNumbers(PNum.Text.ToString());
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            //if (requestCode == (int)Result.Ok)
            //{

            //    int cicco = 1;
                
            //}
        }
    }
    public class SqliteManagerRL
    {
        

        [Table("PriorityNumbers")]
        public class PriorityNumbers
        {
            public string Numero { get; set; }


        }


        SQLiteConnection db;

        private void Conn()
        {
            var dbName = "Numbers.db3";
            var path = System.IO.Path.Combine("/sdcard/", dbName);
            db = new SQLiteConnection(path);
        }

        
        public void Insert_PNumbers(string Numero)
        {
            Conn();
            db.DropTable<PriorityNumbers>();
            db.CreateTable<PriorityNumbers>();

            string query = string.Format("INSERT INTO  PriorityNumbers (numero) VALUES ('{0}') ", Numero);

            db.Query<PriorityNumbers>(query);

        }



    }
}
