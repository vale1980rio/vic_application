﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace RedLine 
{
    [Activity(Label = "AlarmManager")]
    public class AlarmManager : Activity
    {
        public Button StopAlarm;
        public MediaPlayer player;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //impostare un orario in cui è stato lanciato e se esiste un lancio nel periodo di lancio allora non lancio

           

            SetContentView(Resource.Layout.lAlarmManager);

            StopAlarm = FindViewById<Button>(Resource.Id.StopButton);
            StopAlarm.Click += StopAlarm_Click;
            
            StartPlayer("/sdcard/Tornado.mp3");
        }

        private void StopAlarm_Click(object sender, EventArgs e)
        {
            //StopPlayer();
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this.ApplicationContext);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutBoolean("alarm_key", true);
            // editor.Commit();    // applies changes synchronously on older APIs
            editor.Apply();        // applies changes asynchronously on newer APIs

            SqliteManagerRL2 mm = new SqliteManagerRL2();
            mm.Insert_Alarm();


            this.Finish();
        }

        public void StartPlayer(string filePath)
        {
            if (player == null)
            {
                player = new MediaPlayer();
            }
            AudioManager am = (AudioManager)Android.App.Application.Context.GetSystemService(Context.AudioService);

            int maxVol = am.GetStreamMaxVolume(Android.Media.Stream.Music);
            am.SetStreamVolume(Android.Media.Stream.Music, maxVol, VolumeNotificationFlags.PlaySound);
            am.RingerMode = RingerMode.Normal;
            // player.SetVolume(maxVol, maxVol);
            player.Reset();
            player.SetDataSource(filePath);
            player.Prepare();
            //player.Looping = true;
            player.Start();


        }

        public void StopPlayer()
        {
            player.Stop();
        }
    }
    public class SqliteManagerRL2
    {


        [Table("AlarmsAct")]
        public class AlarmsAct
        {
            public string orario { get; set; }


        }


        SQLiteConnection db;

        private void Conn()
        {
            var dbName = "Numbers.db3";
            var path = System.IO.Path.Combine("/sdcard/", dbName);
            db = new SQLiteConnection(path);
        }


        public void Insert_Alarm()
        {
            Conn();
            db.DropTable<AlarmsAct>();
            db.CreateTable<AlarmsAct>();

            string query = string.Format("INSERT INTO  AlarmsAct (orario) VALUES (DATETIME('now')) ");

            db.Query<AlarmsAct>(query);

        }



    }
}